// Code your testbench here
// or browse Examples
module top;
  class A;
    rand int a;
    rand int b;
    
    constraint a_c {a%2==0;}
    constraint b_c {b%a==0;}
  endclass : A
  
  initial begin
    A x;
    x = new();
    x.randomize();
    $display("%d %d", x.a,x.b);
  end
endmodule : top